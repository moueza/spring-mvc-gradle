package moi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.baeldung.contexts.services.GreeterService;

public class MoiController {

	@Autowired
	private GreeterService greeterService;

	@RequestMapping(path = "/moi")
	public ModelAndView helloWorld() {
		return new ModelAndView("moi");
	}

	@RequestMapping(path = "/moi/moi")
	public ModelAndView helloWorld2() {
		return new ModelAndView("moi");
	}

	@RequestMapping(path = "/spring-mvc-gradle/moi/moi")
	public ModelAndView helloWorld3() {
		return new ModelAndView("moi");
	}

	@RequestMapping(path = "/")
	public ModelAndView helloWorld4() {
		return new ModelAndView("moi");
	}

	@RequestMapping(path = "/spring-mvc-gradle/")
	public ModelAndView helloWorld5() {
		return new ModelAndView("moi");
	}
}
